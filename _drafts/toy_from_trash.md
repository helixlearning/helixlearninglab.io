---
layout: post
title:  "Toy From Trash"
date:   2016-11-19 15:39:04
categories: 
---

Toy from Trash – 1
==================

We tought to kids Newton’s third law of motion, using household and waste materials. There are numerous toys available in the market that uses this phenomenon. But making toys by yourself developes creativity and motor skills both fine and gross. This also develops habit of reusing and recycling the things. One thing is sure, it creates mess in the house. 

This time we made ball bouncer taking hints from [this]  (arvindguptatoys.com). Recently creater of this resource Mr. Arvind Gupta got Padmashree Award in India. These have hundreds of DIY toys ideas and its one of our favourite resource of learning. 

To make Ball Bouncer you will just need :

- Empty strong water / juice bottle
- Pair of scissors
- Couple of rubber bands
- Thread 
- A bead
- Table-Tennis ball

Instruction to make this toy are availble [here] (http://www.arvindguptatoys.com/toys/Ballbounce.html).

The result was amazing. Kids loved this toy. They even had competition on who will bounce highest. Interesting part is more than half of kids’ toy didn’t work. So they were disappointed but together we found the solution and learnt. 

> Irony is kids with the faulty toy leant more than kids who made perfect toy as per the instruction.

![Ball Bouncer](/assets/bal_bouncer.jpg "Ball bouncer").

+ Some kids didn’t have beads. Simply pulling the thread down ball didn’t bounce. Because without bead you can not get proper grip on the thread so while leaving the thread it doesn’t produce instant reaction. So then we put the bead for them.

+ Some kids had wide bottles. Table-Tennis ball was too small and bigger one was heavy. The gap between Rubber band cross was too big and it couldn’t rest there. So we had to make 4 more cuts to add 2 more rubber bands by keeping in mid that distance between all cuts should be the same. Here kids learnt the concept of big and small angles, short and long distance, division and different parts of the circle. Wider the bottle, more numer of supporting rubber bands you need to decrease the gap so that ball doesn’t fall down. 

+ Some kids’ bottle quality was too thin.  Mineral water battle were too thin to support tight rubber bands. We needed to add support by sticking tape. It worked but didn’t look perfect. So we made another toy for them called [Ball shooter] (http://www.arvindguptatoys.com/toys/Ballshooter.html) 

 For this you will need just top part of the bottle, balloon and tiny ball made of paper.
 ![Ball Shooter](/assets/ball shooter.png "Ball shooter").
 
