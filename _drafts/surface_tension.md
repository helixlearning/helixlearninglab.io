---
layout: post
title:  "Surface Tension"
date:   2018-1-12 15:39:04
categories: 
---

Surface Tension
===============

Definition 
----------
It is a phenomenon in which the surface of a liquid, where the liquid is in contact with gas, acts like a thin elastic sheet. Water has higer surface tension than many more liquids. As it is abundantly available we did many experiments on this topic. 

### Color Magic

One experiment which kids like more was using food color. 

Things you need: 

+ Food colors
+ Milk
+ Dish
+ Soap water and 
+ Earbud. 

Steps for the experiments: 
1. Take a dish and pore some milk, make sure that surface is horizontal. 
2. Put some drops of food coloring on the milk surface. 
3. Put liquid dish wash on the ear bud.
4. Touch the earbud with liquid dish wash on the milk.
5. See the Magic

![Color magic](/assets/color magic.png "Color magic")


* We did same experiment using pepper powder, tiny paper pieces, turmeric instead of food color.
* After the above experiment we used same water to paint on the ground, kids loved to mess it up.

While doing these fun experiments they observed some facts too, like:

 1. If they put soap drop on just plane water or milk without any color or powder on it, you can not observe anything.
 2. Once you added soap drop same liquid can not give results for the experiments, you have to change the liquid everytime.
