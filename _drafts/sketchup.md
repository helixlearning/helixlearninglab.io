---
layout: post
title:  "SketchUp"
date:   2017-02-17 20:23:04
categories: 
---

[SketchUp](https://www.sketchup.com/) is a 3D drawing app by Google. It's great for children too because it gives them a nice environment to understand the interaction of 3D shapes. They can experiment with manipulation of shapes, shadows, surfaces, holes etc. Here are some of the things our kids made:

![Home SketchUp]({{ "/assets/sketchhome" | absolute_url }})