---
layout: post
title: Averages
date: 2017-12-30 20:42 +0530
---
How should we introduce the concept of averages to kids? The worst approach would be to do so via the formula. Averages are a metric intended to capture the central tendency of a dataset. So, as always, we should start with a concrete example that sticks.

If there are two points, the "center" would the mid-point of the segment connecting them. This is very easy for little kids to grasp. Stand some distance apart, and take equal number of steps. The point you reach is the center point, or the "average".

Then you can take the same concept over to the number line. The middle point between 3 and 7 is 5 because it is at the same distance from both those points.

Only after you have practised finding the average of two numbers like this, should you actually introduce the trick that average = (a + b) / 2.

And all that then remains is to generalize this to more than 2 points. Remember, the core idea is: central tendency.

For extra credit, try introducing medians too. And explain how mean and median behave differently and respond to extreme cases. We haven't covered medians with our kids at Helix yet so stay tuned!