---
layout: post
title:  "Programming For Kids"
date:   2017-02-08 16:23:04
categories: 
---

Computer programming, like most things in mathematics, is very simple at its core. It's all about combining simple instructions to get the computer to do something complex. Like the game of chess. But there are a few new ideas here: loops, jumps, recursion, assignment, data-types etc.

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">My kid really likes this &quot;app&quot; I built for her! 😁 <a href="https://t.co/7bSgil3vyS">pic.twitter.com/7bSgil3vyS</a></p>&mdash; Nilesh Trivedi (@nileshtrivedi) <a href="https://twitter.com/nileshtrivedi/status/860361194380644352?ref_src=twsrc%5Etfw">May 5, 2017</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
