---
layout: post
title: "The Beauty Of Mathematics"
date:  2016-04-02 10:28:14
categories: 
---
Paul Lockhart's famous 25-page essay [A Mathematician's Lament](https://en.wikipedia.org/wiki/A_Mathematician%27s_Lament) was a key influence on us for designing our learning program. You must read it - *especially* if you think that maths isn't a creative activity like making music or painting.

Quoting:

> Waking up in a cold sweat, the musician realizes, gratefully, that it was all just a crazy dream. “Of course!” he reassures himself, “No society would ever reduce such a beautiful and meaningful art form to something so mindless and trivial; no culture could be so cruel to its children as to deprive them of such a natural, satisfying means of human expression. How absurd!”

> ...

> Sadly, our present system of mathematics education is precisely this kind of nightmare. In fact, if I had to design a mechanism for the express purpose of destroying a child’s natural curiosity and love of pattern-making, I couldn’t possibly do as good a job as is currently being done—I simply wouldn’t have the imagination to come up with the kind of senseless, soul-crushing ideas that constitute contemporary mathematics education.