---
layout: post
title:  "Optics"
date:   
categories: 
---

### Optics is 
> the scientific study of sight and the behaviour of light, or the properties of transmission and deflection of other forms of radiation.

This sounds too geeky for young children. But practally optics learning is happening evertime the moment we wake up till we sleep. 
* Observing things in different angles.
* Kids play with shadows.
* Use of torch in dark.
* comparing the level of sunlight we get on plane ground, on porch area, ventilated room and basements.
* While watching movies on laptop we adjust the screen for clear view.

Kids don't really know they are learning something. By using simple tools we did same as above in fun and conscious way. 

There is a simple game we used to play, shadow game. In the Evening while playing on the street with multiple street light around and the play was:
* Hide the shadow:

One kid will run around and others will try to hide his/her shadow by running around him/her. 

* Compare the shadow:

Comparing the shadow standing at same level and or at different levels. 

* Flip the shadow:

Because there were multiple street lights around depending on which area is coverd by which light most, your shadow will get flip. And that was the fun game too.


We got [this] (https://www.amazon.in/Kutuhal-Easy-Optics-Physics-Experiment/dp/B00J4M3C2I) optics DIY learning kit and 5mW green laser pointer from Amazon. As per the manual this kit is for 8th to 12th std. Students. But we still got it for our daughter and it was a super success. 

This kit contains :
* convex and concave mirrors and lens
* glass slab and prism
* torch etc.
* Instrutions manual

With few important instructions and precautions we started playing. One of them is **‘DON’T PUT LASER POINTER RAYS DIRECTLY IN EYES.’** Manual contains lots of simple experiments to learn
* Reflection
* Refraction
* Refractive index
* Dispersion
* Interference and Diffraction
* Intensity

Some of these terms sounds complicated so without using such words we just had fun playing with it. Some simple experiments are as follows:

1. Take a laser pointer and throw the beam on mirror. See the reflection of light through different angles.
2. Throw the beam through glass slab we can see diversion of light ray.
3. Pass the laser beam through prism and see the effects. 
4. Pass the laser beam through convex and concave lenses and see the effects.

As of now this was enough for her to take this and admire it completely. After few days we will do these experiments again with more details. 