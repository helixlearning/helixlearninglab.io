---
layout: post
title: Graph Theory For Kids!
date: 2017-12-30 20:51 +0530
---

Joel David Hamkins wrote [this delightful post](http://jdh.hamkins.org/math-for-eight-year-olds/) on how he introduced 8-year olds to graph theory. We decided to try it out with even younger kids here at Helix.

First we introduced the notion of a graph: A set of vertices connected by edges which may be directed/undirected and may have unequal weights. Of course we didn't tell the kids this abstract definition because kids need something concrete to make it sticky. So we talked about all the cities the child has travelled to and how they're connected via road, rail, or air. Here, cities are the vertices and they are connected by edges. Distance between two cities is the weight of the corresponding edge.

Another example of graphs is: social networks, i.e. friends and family. A has B as a friend, and B has C as a friend.

This led us to discuss different types of graphs. Parent-children relationships create a graph called family-tree. In computer science, "tree" means something different (every child can have only one parent). We discussed why it is called a "tree" (flip it vertically and see how it resmbles an actual tree with branches). To explain directed graphs, we discussed one-way roads in a city.

This was plenty of material to chew on for day 1.

After a few days, we revised all this again and started noticing graphs in more situations. Your flat is a graph if you think of rooms as vertices and doors as edges. This is really what true mathematics is about: identifying patterns.

We also went through the notion of regions - as explained in Hamkins' booklet. We drew all sorts of graphs with paper & pencil, and counted the number of vertices, edges and regions in each. A key concept to understand here was that the same graph can be drawn on paper in many different ways. The kids quickly understoof the difference between connected and unconnected graphs.

Now, I asked them to compute V - E + R for any graph they could make and see if they see any pattern. The kids didn't know it, but they were discovering [Euler's characteristic for planar graphs](https://en.wikipedia.org/wiki/Euler_characteristic#Plane_graphs). They did notice the pattern and also noticed how it breaks down for unconnected graphs.

The proof of this theorem is amazingly simple and just needs common sense and principle of induction, but the kids are a bit too young for that! If you're curious, quoting here from the linked post:

The idea of the proof is that V−E+R=2 is true at the start, in the case of a graph consisting of one vertex and no edges. Furthermore, it remains true when one adds one new vertex connected by one new edge, since the new vertex and new edge cancel out.  Also, it remains true when one carves out a new region from part of an old region with the addition of a single new edge, since in this case there is one new edge and one new region, and these also balance each other. Since any connected planar graph can be built up in this way by gradually adding new vertices and edges, this argument shows V−E+R=2 for any connected planar graph. This is a proof by mathematical induction on the size of the graph.

The Euler characteristic can be generalized to 3D solids (here we have surfaces, instead of regions). We tried computing V-E+F for many solid shapes: Cube, Triangular Prism, Tetrahedron, Pyramid. And what do you know: the same answer 2 comes here every time!

