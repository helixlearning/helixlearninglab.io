---
layout: post
title:  "Number Circles (Modular Arithmetic For Kids)"
date:   2016-09-25 17:19:04
categories: 
---
Many of us are familiar with the number line. But what if we put numbers on a circle?

This isn't a complicated idea. You are already familiar with it: Analog clocks. If current time is 9 o' clock, 4 hours later it will be 1 o'clock. Numbers just wrap around 12 which is considered equivalent to 0.

![Clock]({{ "/assets/clock.svg" | absolute_url }})

Mathematicians know this as [Modular Arithmetic](https://en.wikipedia.org/wiki/Modular_arithmetic). But the idea is simple enough that our 4 year olds could grasp and master this!

Introducing this idea at a young age is very beneficial. It emphasizes to the childern's mind that there are multiple ways to "add" or "subtract" things (i.e. do arithmetic). This prepares them for topics such as vectors, complex numbers and abstract algebra. Operations become just another entity to manipulate!

Modular arithmetic - when extended to alphabets - also leads to basics of cryptography, such as, rotation ciphers. Again, all this might sound complicated but is actually so simple that our 4 year olds are able to send encrypted messages to their friends!

Another surprising place where number circles show up is music! Observe the pattern of keys on the piano:

Or notice the notes in solfege: Sa Re Ga Ma Pa Dha Ni Sa. See how the sequence returns back to Sa and such circles keep repeating?

So, again with rules of modular addition, the second note after Ni in major scale is Re. Mathematicians would write this as: 6 + 2 = 1 mod 7. But at Helix we don't worry about the notation and the jargon so early. Instead we prefer to just play with the ideas! :-)