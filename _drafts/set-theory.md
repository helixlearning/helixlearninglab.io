---
layout: post
title: Set Theory For Kids
---

If Helix kids could handle graph theory, surely they can handle set theory too! It turned out to be rather easy. It's also rather important for building logical acumen. Syllogistic logic, for example, can be fully understood with nothing but set theory and venn diagrams.

We started with some YouTube videos about sets and what it means to be an element. Remember the principle for teaching anything to kids: Experience first, jargon later.

We started diagramming sets on paper and placing items in the correct set. This is very important for what follows next.

Then came the set operations: intersection, union, and complement. I found it very helpful to take the help of English language. Intersection of sets A and B refers to things which are *both* A *and* B. Union of A and B = EITHER A OR B. Complement of A = NOT A.

The operations can also be combined together. Complement of the union of A and B = NEITHER A NOR B.

All of this is building up to one of the nicest theorems in set theorem ins DeMorgan's laws. But at this point, the kids got distracted and went playing with soap bubbles. :-)

