---
layout: post
title:  "We Need Help"
date:   2017-12-28 11:53:09
categories: 
---

Till now, we used to buy all the tools and equipment for Helix kids' learning from our own pockets but now we need donations to be able to build the best environment. Specifically, we need help to:

  - Books (Here's our wishlist)
  - An iPad Pro (with Pencil)
  - Science kits
  - Math manipulatives
  - Field trips