---
layout: post
title: Tetrahedron
date: 2018-02-14 20:41 +0530
---

Tetrahedron
========
### defination
> Tetrahedron is a polyhedron composed of four triangular faces. It is  also called as triangular pyramid. 

It is an amazing 3 diamentional model. Using this as a basic unit we can make lots of complex structures. like:
* Sierpinski pyramid
* Tetrahedral-octahedral honeycomb
* Stellated octahedron

and many more.

To achieve this we started to make tetrahedral and in the process we found out many ways of making it.

1. ![tetrahedral templet](/assets/Tetrahedrontemplate.GIF "Tetrahedron") Simplest way is to just print - cut - glue it. 

2. ![tetrahedral](/assets/Tetrahedron.png "Tetrahedron") Another way is make 3 circles and join the intersecting points. More details about it are available [here] (http://www.mathcats.com/crafts/tetrahedron.html).

3. Make one using sonobe unit. [like this] (http://www.instructables.com/id/How-to-Make-a-Triangular-Hexahedron-out-of-Paper-S/)
