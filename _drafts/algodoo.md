---
layout: post
title:  "Algodoo"
date:   2016-12-23 15:39:04
categories: 
---

[Algodoo](http://www.algodoo.com/) is an amazing interactive 2D physics simulation playground app. Not only can you build a scene and simulate but also manipulate it with keyboard shortcuts and mouse. There's also an [online community](http://www.algodoo.com/algobox/) where people can share their creations and some of those are absolutely fascinating! Games, machines, art - you name it, it's there! This short video is the best way to explain what this app can do:

<iframe width="560" height="315" src="https://www.youtube.com/embed/0LGzTKINqJk" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe><br/><br/>

Here's a screenshot:
![Algodoo screenshot]({{ "/assets/algodoo.png" | absolute_url }})

Our kids particularly liked creating buckets of liquid and then cutting them via the knife tool. Or attaching thrusters (rocket engines) to objects and controlling them via keyboard shortcuts. Here are some scenes made by Helix kids:

Nilesh had once himself [created an interactive physics simulation engine](https://hasgeek.tv/jsfoo/2013-2/688-interactive-physics-simulation-in-the-browser-what-i-learned) for the browser. Feel free to bug him if you'd like to know how such an app actually works. Careful! - it will be hard to make him stop once you broach this subject. :-)