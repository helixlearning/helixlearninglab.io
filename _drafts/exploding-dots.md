---
layout: post
title:  "Exploding Dots"
date:   2017-05-12 14:08:14
categories: 
---
Our children had such a great time playing with the ideas in [James Tanton's](https://twitter.com/jamestanton) fantastic video "Exploding Dots" about the place-value system.

<iframe src="https://player.vimeo.com/video/204368634" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/204368634">Exploding Dots</a> from <a href="https://vimeo.com/azmath">AZ Mathematics Partnership</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

Who knew that factorising and dividing polynomials could be this easy and fun! :-)