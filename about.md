---
layout: page
title: About
permalink: /about/
---

Helix is an experiential learning (learning by doing) space for young children in Bangalore. We have been building this step-by-step since 2013.

We believe in active learning - which happens by observing/imitating experts, trying to do it yourself and by teaching beginners. We also expose kids to the beauty of mathematics through games, puzzles, origami, and interactive tools. In the process, we subtly train them in mathematical thinking. Our children are way ahead in understanding advanced maths concepts compared to their peers. But they think it's all just fun & games! :-)

[Read this post]({% link _posts/2016-03-24-how-we-learn-at-helix.md %}) to know more about our methodology and beliefs.

We are located in Koramangala 4th Block, Bengaluru. Please drop us an email at <a href="mailto:{{ site.email }}">{{ site.email }}</a> before visiting.