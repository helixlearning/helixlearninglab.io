---
layout: post
title:  "How We Learn At Helix"
date:   2016-03-24 15:32:14
categories: 
---
Having clarity about the aims of education is essential before one can go about designing a well-rounded program. For early childhood, we particularly like the view espoused by famous philosopher Bertrand Russell who argued that the primary aims of education ought to be: (1) Vitality, (2) Courage, (3) Sensitivity and (4) Intelligence.

[Russell's book](https://www.goodreads.com/book/show/1179528.On_Education) is highly recommended to understand this view in detail. [This is a good summary](http://www.yourarticlelibrary.com/education/bertrand-russell-view-on-education/69148).

At Helix, we also believe that learning how to _think_ correctly, and learning how to _learn_ anything are far more critical skills to aquire than to learn any specific set of facts. We expose the children to various physical and social phenomena, but we do *NOT* provide an explanation. The idea is for the child to be able to come up with potential theories and evalue them by himself/herself.

We also incorporate latest research from psychology and cognitive science in our program design. Examples include [Carol Dweck's "Fixed vs Growth Mindset"](https://www.youtube.com/watch?v=_X0mgOOSpLU) or the [importance of meta-cognition](https://www.nap.edu/catalog/9853/how-people-learn-brain-mind-experience-and-school-expanded-edition). BTW, adults too can benefit from these ideas. [See this example](http://nautil.us/issue/17/big-bangs/how-i-rewired-my-brain-to-become-fluent-in-math).